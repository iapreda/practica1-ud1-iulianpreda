package com.ipreda.ordenadores.base;

public class Ordenadores {


    /**
     * Creación de atributos que definen la clase "Ordenador"
     */
    private String marca;
    private String modelo;
    private String procesador;
    private String placaBase;
    private String memoriaRam;
    private String tarjetaGrafica;
    private String precio;


    /**
     * Constructor que recibe datos de la clase "TiendaOrdenadores"
     * @param marca
     * @param modelo
     * @param procesador
     * @param placaBase
     * @param memoriaRam
     * @param tarjetaGrafica
     * @param precio
     */

    public Ordenadores(String marca, String modelo, String procesador, String placaBase, String memoriaRam, String tarjetaGrafica, String precio){
        this.marca = marca;
        this.modelo = modelo;
        this.procesador = procesador;
        this.placaBase = placaBase;
        this.memoriaRam = memoriaRam;
        this.tarjetaGrafica = tarjetaGrafica;
        this.precio = precio;

    }

    /**
     * Getter de "Marca"
     *
     * @return devuelve el campo Marca.
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Setter de "Marca"
     *
     * @param marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Getter de Modelo.
     *
     * @return devuelve el valor de modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Setter de Modelo
     *
     * @param modelo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    /**
     * Getter de Procesador
     *
     * @return devuelve el valor del campo procesador
     */
    public String getProcesador() {
        return procesador;
    }

    /**
     * Setter de procesador
     *
     * @param procesador
     */
    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    /**
     * Getter de placaBase
     *
     * @return devuelve el valor introducido en el campo
     */
    public String getPlacaBase() {
        return placaBase;
    }

    /**
     * Setter de placaBase
     *
     * @param placaBase
     */
    public void setPlacaBase(String placaBase) {
        this.placaBase = placaBase;
    }

    /**
     * Getter de memoria ram
     *
     * @return devuelve el valor del campo
     */
    public String getMemoriaRam() {
        return memoriaRam;
    }

    /**
     * Setter de memoriaRam
     *
     * @param memoriaRam
     */
    public void setMemoriaRam(String memoriaRam) {
        this.memoriaRam = memoriaRam;
    }

    /**
     * Getter de tarjeta grafica
     *
     * @return devuelve el valor del campo
     */
    public String getTarjetaGrafica() {
        return tarjetaGrafica;
    }

    /**
     * Setter de tarjeta grafica
     *
     * @param tarjetaGrafica
     */
    public void setTarjetaGrafica(String tarjetaGrafica) {
        this.tarjetaGrafica = tarjetaGrafica;
    }

    /**
     * Getter de precio
     *
     * @return devuelve el valor introducido en el campo precio
     */
    public String getPrecio() {
        return precio;
    }

    /**
     * Setter de precio
     *
     * @param precio
     */
    public void setPrecio(String precio) {
        this.precio = precio;
    }


    /**
     * Metodo toString de todos los campos
     *
     * @return devuelve datos introducidos previamente en cada campo
     */
    @Override
    public String toString() {
        return "MARCA: " + marca + "\n" +
                "MODELO: " + modelo   + "\n" +
                "PROCESADOR: " + procesador + "\n" +
                "MODELO PLACA BASE: " + placaBase + "\n" +
                "MEMORIA RAM INSTALADA: " + memoriaRam + "\n" +
                "TARJETA GRAFICA: " + tarjetaGrafica + "\n" +
                "PRECIO TOTAL: " + precio;
    }
}
