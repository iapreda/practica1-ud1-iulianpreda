package com.ipreda.ordenadores.tienda;

import com.ipreda.ordenadores.base.Ordenadores;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class TiendaOrdenadores {

    private JTextField txMarca;
    private JTextField txModelo;
    private JTextField txProcesador;
    private JTextField txPlaca;
    private JTextField txRam;
    private JTextField txGrafica;
    private JTextField txPrecio;
    private JComboBox cBox;
    private JButton bAlta;
    private JButton bModificar;
    private JButton bMostrar;
    private JButton bSalirPrograma;
    private JPanel yulipcshop;
    private JLabel lbPc;
    private JTextArea txAr;
    private JFrame marco;


    private LinkedList<Ordenadores> listado;
    private DefaultComboBoxModel<Ordenadores> dcbm;


    /**
     * Metodo que añade las caracteristicas del marco y crea los botones funcionales.
     */
    public TiendaOrdenadores() {

        marco = new JFrame("YULI PC-SHOP");
        marco.setContentPane(yulipcshop);
        marco.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        marco.pack();
        marco.setSize(1000, 600);
        marco.setVisible(true);

        creacionMenu();

        marco.setLocationRelativeTo(null);
        listado = new LinkedList<>();
        dcbm = new DefaultComboBoxModel<>();
        cBox.setModel(dcbm);

        bAlta.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaOrdenador(txMarca.getText(), txModelo.getText(), txProcesador.getText(), txPlaca.getText(), txRam.getText(), txGrafica.getText(), txPrecio.getText());
                refrescarCBox();

            }
        });

        bMostrar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Ordenadores pcs = (Ordenadores) dcbm.getSelectedItem();
                lbPc.setText(pcs.toString());
                txAr.setText(pcs.toString());

            }
        });


        bSalirPrograma.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
    }

    /**
     * Metodo para refrescar el comboBox. Sirve para actualizar la información ingresada.
     */
    private void refrescarCBox() {
        dcbm.removeAllElements();
        for (Ordenadores ordenadores : listado) {
            dcbm.addElement(ordenadores);
        }
    }

    public static void main(String[] args) {
        TiendaOrdenadores pcs = new TiendaOrdenadores();
    }


    /**
     * Metodo para crear el menu con la barra correspondiete.
     * Añade dos opciones "Importar y exportar"
     */
    private void creacionMenu() {


        // Creación de la opción "Archivo" para la bara del menú con dos opciones. Exportar e Importar.
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");


        // Método para exportar un archivo, en este caso XML.
        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser seleccionarArchivo = new JFileChooser();
                int opcionSeleccionada = seleccionarArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = seleccionarArchivo.getSelectedFile();
                    exportarXML(fichero);
                }
            }
        });


        // Método para importar un archivo.
        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser seleccionarArchivo = new JFileChooser();
                int opcionSeleccionada = seleccionarArchivo.showOpenDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION) {
                    File fichero = seleccionarArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarCBox();
                }
            }
        });


        // Se añaden las opciones de "Importar y Exportar" a la barra de menu.
        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        barra.add(menu);
        marco.setJMenuBar(barra);


    }

    /**
     * Metodo para agregar datos al constructor que se encuentra en "Ordenadores"
     *
     * @param marca
     * @param modelo
     * @param procesador
     * @param placaBase
     * @param memoriaRam
     * @param tarjetaGrafica
     * @param precio
     */
    private void altaOrdenador(String marca, String modelo, String procesador, String placaBase, String memoriaRam, String tarjetaGrafica, String precio) {
        listado.add(new Ordenadores(marca, modelo, procesador, placaBase, memoriaRam, tarjetaGrafica, precio));
    }


    /**
     * Metodo para la exportacion de datos creando un fichero.
     * Recibe los datos del metodo "exportar".
     *
     * @param fichero
     */
    private void exportarXML(File fichero) {

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            Document doc = dom.createDocument(null, "xml", null);

            Element raiz = doc.createElement("Ordenadores");
            doc.getDocumentElement().appendChild(raiz);

            Element nodoOrdenador;
            Element nodoDatos;

            Text dato;

            for (Ordenadores ordenadores : listado) {

                //Se crea el nodo Ordenador

                nodoOrdenador = doc.createElement("ordenador");
                raiz.appendChild(nodoOrdenador);

                //Añado el nodo marca al nodo ordenador
                nodoDatos = doc.createElement("Marca");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getMarca());
                nodoDatos.appendChild(dato);


                //Añado el nodo Modelo al nodo ordenador
                nodoDatos = doc.createElement("Modelo");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getModelo());
                nodoDatos.appendChild(dato);

                //Añado el nodo Procesador al nodo ordenador
                nodoDatos = doc.createElement("Procesador");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getProcesador());
                nodoDatos.appendChild(dato);

                //Añado el nodo PlacaBse al nodo ordenador
                nodoDatos = doc.createElement("PlacaBase");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getPlacaBase());
                nodoDatos.appendChild(dato);


                //Añado el nodo MemoriaRam al nodo ordenador
                nodoDatos = doc.createElement("MemoriaRAM");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getMemoriaRam());
                nodoDatos.appendChild(dato);


                //Añado el nodo tarjetaGrafica al nodo ordenador
                nodoDatos = doc.createElement("TarjetaGrafica");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getTarjetaGrafica());
                nodoDatos.appendChild(dato);

                //Añado el nodo marca al nodo ordenador
                nodoDatos = doc.createElement("Precio");
                nodoOrdenador.appendChild(nodoDatos);

                //Añado el dato al nodo de datos.
                dato = doc.createTextNode(ordenadores.getPrecio());
                nodoDatos.appendChild(dato);
            }

            Source src = new DOMSource(doc);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }


    }

    /**
     * Metodo para importar un fichero exportado de forma previa.
     *
     * @param fichero
     */

    private void importarXML(File fichero) {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document documento = db.parse(fichero);

            NodeList ordenadores = documento.getElementsByTagName("ordenador");

            // Bucle para sacar todos los datos de los nodos y asi obtener sus campos.
            for (int i = 0; i < ordenadores.getLength(); i++) {
                Node ordenador = ordenadores.item(i);
                Element element = (Element) ordenador;

                // Obtencion de los campos descritos abajo.
                String marca = element.getElementsByTagName("Marca").item(0).getChildNodes().item(0).getNodeValue();
                String modelo = element.getElementsByTagName("Modelo").item(0).getChildNodes().item(0).getNodeValue();
                String procesador = element.getElementsByTagName("Procesador").item(0).getChildNodes().item(0).getNodeValue();
                String placaBase = element.getElementsByTagName("PlacaBase").item(0).getChildNodes().item(0).getNodeValue();
                String memoriaRam = element.getElementsByTagName("MemoriaRAM").item(0).getChildNodes().item(0).getNodeValue();
                String tarjetaGrafica = element.getElementsByTagName("TarjetaGrafica").item(0).getChildNodes().item(0).getNodeValue();
                String precio = element.getElementsByTagName("Precio").item(0).getChildNodes().item(0).getNodeValue();


                altaOrdenador(marca, modelo, procesador, placaBase, memoriaRam, tarjetaGrafica, precio);

            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}


